import os
import configparser

DB = "postgresql+psycopg2://{user}:{password}@{host}/{db}"

parser = configparser.ConfigParser()
parser.read("database/conn_setup.ini")

CONF = {"user": "ltv", "db": "ltv"}
CONF.update(parser["ltv"])
parser.clear()


class Config(object):
    """Base configuration."""

    FLASK_INIT = {"template_folder": "../../frontend/production/views"}
    SECRET_KEY = os.environ.get('FLASK_SECRET', 'secret-key')
    SELF_PATH = os.path.realpath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.realpath(os.path.join(SELF_PATH, os.pardir))
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Disable emitting signals


class ProdConfig(Config):
    """Production configuration."""

    ENV = 'prod'
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/example'
    DEBUG_TB_ENABLED = False  # Disable Debug toolbar


class DevConfig(Config):
    """Development configuration."""

    FLASK_INIT = {
        "template_folder": "../../frontend/production/views",
        "static_folder": "../../frontend/production/static"
    }
    ENV = 'dev'
    DEBUG = True
    # DB_NAME = 'dev.db'
    # Put the db file in project root
    # DB_PATH = os.path.join(Config.PROJECT_ROOT, "database", DB_NAME)
    SQLALCHEMY_DATABASE_URI = DB.format(**CONF)
    DEBUG_TB_ENABLED = True
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.


class TestConfig(Config):
    """Test configuration."""

    FLASK_INIT = {
        "template_folder": "../../frontend/production/views",
        "static_folder": "../../frontend/production/static"
    }
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    WTF_CSRF_ENABLED = False  # Allows form testing
