from setuptools import setup

setup(
    name='long-time-variations',
    version="1.0.0",
    packages=['page'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask-sqlalchemy',
        'flask-debugtoolbar'
    ],
)
