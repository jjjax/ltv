from flask import render_template
from flask import Blueprint
from extensions import db

from database.orm.ltv import Detectors

blueprint = Blueprint(
    'page',
    __name__
)


@blueprint.route('/')
def index():
    detectors = db.session().query(Detectors)
    return render_template("info.jinja2", detectors=detectors)
