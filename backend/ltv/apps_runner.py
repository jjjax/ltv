from flask import Flask
from flask.helpers import get_debug_flag

from page import controllers as page_controllers
from settings import DevConfig, ProdConfig
from extensions import db, debug_toolbar


def create_app(config_object=ProdConfig):
    """An application factory. """
    app = Flask(__name__.split('.')[0], **config_object.FLASK_INIT)
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    # register_errorhandlers(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    db.init_app(app)
    debug_toolbar.init_app(app)
    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(page_controllers.blueprint)
    return None


# def register_errorhandlers(app):
#     """Register error handlers."""
#     def render_error(error):
#         """Render error template."""
#         # If a HTTPException, pull the `code` attribute; default to 500
#         error_code = getattr(error, 'code', 500)
#         return render_template('{0}.html'.format(error_code)), error_code
#     for errcode in [401, 404, 500]:
#         app.errorhandler(errcode)(render_error)
#     return None


CONFIG = DevConfig if get_debug_flag() else ProdConfig

application = create_app(CONFIG)

if __name__ == "__main__":
    application.run()
