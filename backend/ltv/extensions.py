"""Extensions module. Each extension is
initialized in the app factory located in app.py."""
from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
debug_toolbar = DebugToolbarExtension()
