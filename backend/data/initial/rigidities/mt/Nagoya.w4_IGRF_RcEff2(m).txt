1950      8.3160       7.003       9.903      2.9000      8.3150
1951      8.3210       6.855       9.910      3.0550      8.3200
1952      8.3430       6.859       9.681      2.8220      8.3420
1953      8.3660       7.033       9.693      2.6600      8.3650
1954      8.3880       7.172       9.936      2.7640      8.3870
1955      8.3980       7.044       9.944      2.9000      8.3970
1956      8.3990       6.742       9.715      2.9730      8.3980
1957      8.3850       7.064       9.718      2.6540      8.3840
1958      8.3950       6.850       9.950      3.1000      8.3940
1959      8.3860       7.186       9.724      2.5380      8.3850
1960      8.3770       7.112       9.729      2.6170      8.3760
1961      8.3920       6.795       9.735      2.9400      8.3910
1962      8.4050       7.197      10.043      2.8460      8.4040
1963      8.4220       6.798       9.746      2.9480      8.4210
1964      8.4350       7.059       9.752      2.6930      8.4340
1965      8.4130       7.060       9.756      2.6960      8.4120
1966      8.4290       7.060       9.757      2.6970      8.4280
1967      8.4380       7.144       9.758      2.6140      8.4370
1968      8.4630       7.085       9.760      2.6750      8.4620
1969      8.4980       6.798      10.050      3.2520      8.4970
1970      8.5190       7.146       9.761      2.6150      8.5180
1971      8.5240       6.593       9.760      3.1670      8.5230
1972      8.5410       7.057       9.760      2.7030      8.5400
1973      8.5460       7.125      10.041      2.9160      8.5450
1974      8.5660       7.414       9.758      2.3440      8.5650
1975      8.5760       7.144       9.760      2.6160      8.5750
1976      8.6020       7.119       9.765      2.6460      8.6010
1977      8.6100       7.127       9.770      2.6430      8.6090
1978      8.6470       7.132       9.775      2.6430      8.6460
1979      8.6540       7.226       9.780      2.5540      8.6530
1980      8.6360       7.135       9.779      2.6440      8.6350
1981      8.5960       7.128       9.772      2.6440      8.5950
1982      8.5190       7.123       9.766      2.6430      8.5180
1983      8.5250       6.837       9.759      2.9220      8.5240
1984      8.5070       7.112       9.752      2.6400      8.5060
1985      8.4580       7.085       9.742      2.6570      8.4570
1986      8.4540       7.113       9.728      2.6150      8.4530
1987      8.4360       7.102       9.713      2.6110      8.4350
1988      8.4180       7.076       9.699      2.6230      8.4170
1989      8.4590       7.066       9.684      2.6180      8.4580
1990      8.5040       7.054       9.667      2.6130      8.5030
1991      8.6190       7.113       9.646      2.5330      8.6180
1992      9.0470       6.996       9.625      2.6290      9.0460
1993      9.4980       6.779       9.603      2.8240      9.4970
1994      9.2540       7.026       9.581      2.5550      9.2530
1995      9.1910       7.015       9.559      2.5440      9.1900
1996      9.2120       6.990       9.536      2.5460      9.2110
1997      9.1940       6.670       9.513      2.8430      9.1930
1998      9.2410       7.112       9.490      2.3780      9.2400
1999      9.2900       6.961       9.466      2.5050      9.2890
2000      9.3130       6.730       9.447      2.7170      9.3120
2001      9.3220       6.725       9.432      2.7070      9.3210
2002      9.3170       6.942       9.417      2.4750      9.3160
2003      9.3180       6.937       9.402      2.4650      9.3170
2004      9.3110       6.711       9.745      3.0340      9.3100
2005      9.3070       6.925       9.370      2.4450      9.3060
2006      9.3020       6.627       9.351      2.7240      9.3010
2007      9.2920       6.922       9.703      2.7810      9.2910
2008      9.2760       6.630       9.313      2.6830      9.2750
2009      9.2550       6.896       9.293      2.3970      9.2540
2010      9.2400       6.894       9.273      2.3790      9.2390
2011      9.2250       6.889       9.639      2.7500      9.2240
2012      9.2060       6.617       9.229      2.6120      9.2050
2013      9.1910       6.604       9.608      3.0040      9.1900
2014      9.1740       6.602       9.530      2.9280      9.1730
2015      9.1590       6.664       9.574      2.9100      9.1580
2016      9.1450       6.668       9.501      2.8330      9.1440
2017      9.1350       6.863       9.487      2.6240      9.1340
2018      9.1190       6.897       9.529      2.6320      9.1180
2019      9.0980       6.864       9.457      2.5930      9.0970
2020      9.0820       6.955       9.499      2.5440      9.0810
2021      9.0600       6.444       9.425      2.9810      9.0590
2022      9.0440       6.445       9.465      3.0200      9.0430
2023      9.0210       6.895       9.448      2.5530      9.0200
2024      9.0030       6.528       9.431      2.9030      9.0020
2025      8.9850       6.589       9.414      2.8250      8.9840
2026      8.9600       6.588       9.395      2.8070      8.9590
2027      8.9400       6.928       9.377      2.4490      8.9390
2028      8.9140       6.587       9.358      2.7710      8.9130
2029      8.8900       6.448       9.340      2.8920      8.8890
2030      8.8670       6.618       9.320      2.7020      8.8660
2031      8.8400       6.829       9.299      2.4700      8.8390
2032      8.8170       6.444       9.278      2.8340      8.8160
2033      8.7910       6.858       9.257      2.3990      8.7900
2034      8.7610       6.451       9.237      2.7860      8.7600
2035      8.7290       6.451       9.213      2.7620      8.7280
2036      8.6930       6.446       9.190      2.7440      8.6920
2037      8.6650       6.415       9.167      2.7520      8.6640
2038      8.6390       6.448       9.143      2.6950      8.6380
2039      8.5980       6.461       9.118      2.6570      8.5970
2040      8.5650       6.448       9.094      2.6460      8.5640
2041      8.5300       6.599       9.067      2.4680      8.5290
2042      8.4960       6.811       9.040      2.2290      8.4950
2043      8.4610       6.235       9.012      2.7770      8.4600
2044      8.4200       6.460       8.984      2.5240      8.4190
2045      8.3840       6.597       8.956      2.3590      8.3830
2046      8.3440       6.808       8.926      2.1180      8.3430
2047      8.3050       6.596       8.895      2.2990      8.3040
2048      8.2730       6.595       8.862      2.2670      8.2720
2049      8.2440       6.229       8.829      2.6000      8.2430
2050      8.2170       6.595       8.797      2.2020      8.2160
