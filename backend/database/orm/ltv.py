from sqlalchemy import (
    MetaData,
    Column,
    ForeignKeyConstraint,
    String,
    Float,
    Integer,
    DateTime
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

__all__ = [
    "Detectors",
    "Directions",
    "Rigidities",
    "Data",
    "GSMResults",
    "EarthParameters",
    "SolarParameters"
]

LTVBase = declarative_base(name="LTVBase", metadata=MetaData(schema='ltv'))


class Detectors(LTVBase):
    __tablename__ = "detectors"

    _REPR = (
        "{0}(det_name='{1!r}', lat={2!r}, lon={3!r}, p0={4!r})"
    )

    det_name = Column(
        String(30),
        primary_key=True
    )
    lat = Column(
        Float,
        nullable=False
    )
    lon = Column(
        Float,
        nullable=False
    )
    p0 = Column(
        Float,
        nullable=False
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.det_name, self.lat, self.lon, self.p0
        )


class Directions(LTVBase):
    __tablename__ = "directions"
    __table_args__ = (
        ForeignKeyConstraint(["det_name"], ["detectors.det_name"]),
    )

    _REPR = "{0}(det_name={1!r}, dir_name={2!r}, theta={3!r}, depth={4!r})"

    det_name = Column(
        String(30),
        primary_key=True
    )
    dir_name = Column(
        String(30),
        primary_key=True
    )
    theta = Column(
        Float,
        nullable=False
    )
    depth = Column(
        Float,
        nullable=False
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.det_name, self.dir_name, self.theta,
            self.depth
        )


class Rigidities(LTVBase):
    __tablename__ = "rigidities"
    __table_args__ = (
        ForeignKeyConstraint(
            ["det_name", "dir_name"],
            ["directions.det_name", "directions.dir_name"]
        ),
    )

    _REPR = (
        "{0}("
        " det_name={1!r},"
        " dir_name={2!r},"
        " dt={3!r},"
        " rc={4!r}, rm={5!r}, re={6!r}"
        ")"
    )

    det_name = Column(
        String(30),
        primary_key=True
    )
    dir_name = Column(
        String(30),
        primary_key=True
    )
    dt = Column(
        DateTime(timezone=False),
        primary_key=True
    )
    rc = Column(
        Float(),
        nullable=False,
        default=0
    )
    rm = Column(
        Float(),
        nullable=False,
        default=0
    )
    re = Column(
        Float(),
        nullable=False,
        default=0
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.det_name, self.dir_name, self.dt,
            self.rc, self.rm, self.re
        )


class Data(LTVBase):
    __tablename__ = "data"
    __table_args__ = (
        ForeignKeyConstraint(
            ["det_name", "dir_name"],
            ["directions.det_name", "directions.dir_name"]
        ),
    )

    _REPR = (
        "{0}(det_name={1!r}, dir_name={2!r}, dt={3!r}, c={4!r},"
        " det={5!r}, dir={6!r}, rig={7!r})"
    )

    det_name = Column(
        String(30),
        primary_key=True
    )
    dir_name = Column(
        String(30),
        primary_key=True
    )
    dt = Column(
        DateTime(timezone=False),
        primary_key=True
    )
    c = Column(
        Float(),
        nullable=False,
        default=0
    )
    eff_mod = Column(
        Float(),
        nullable=False,
        default=0
    )
    sig_mod = Column(
        Float(),
        nullable=False,
        default=0
    )
    eff_rel = Column(
        Float(),
        nullable=False,
        default=0
    )
    sig_rel = Column(
        Float(),
        nullable=False,
        default=0
    )

    det = relationship(
        "Detectors",
        primaryjoin="foreign(Data.det_name) == remote(Detectors.det_name)",
        innerjoin=True,
        lazy="joined",
        uselist=False,
    )

    dir = relationship(
        "Directions",
        innerjoin=True,
        lazy="joined",
        uselist=False
    )

    rig = relationship(
        "Rigidities",
        primaryjoin=(
            "and_("
            " foreign(Data.det_name) == remote(Rigidities.det_name),"
            " foreign(Data.dir_name) == remote(Rigidities.dir_name),"
            " foreign(Data.dt) == remote(Rigidities.dt)"
            ")"
        ),
        innerjoin=True,
        lazy="joined",
        uselist=False  # one-to-one
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.det_name, self.dir_name, self.dt,
            self.c, self.det, self.dir, self.rig
        )


class GSMResults(LTVBase):
    __tablename__ = "gsm_results"

    _REPR = (
        "{0}("
        " dt={1!r},"
        " a_gsm={2!r},"
        " sig_a_gsm={3!r},"
        " a10={4!r},"
        " sig_a10={5!r},"
        " b={6!r},"
        " sig_b={7!r},"
        " g={8!r},"
        " sig_g={9!r},"
        " sigma={10!r}"
        ")"
    )

    dt = Column(
        DateTime(timezone=False),
        primary_key=True
    )
    a_gsm = Column(
        Float(),
        nullable=False,
    )
    sig_a_gsm = Column(
        Float(),
        nullable=False,
    )
    a10 = Column(
        Float(),
        nullable=False,
    )
    sig_a10 = Column(
        Float(),
        nullable=False,
    )
    b = Column(
        Float(),
        nullable=False,
    )
    sig_b = Column(
        Float(),
        nullable=False,
    )
    g = Column(
        Float(),
        nullable=False,
    )
    sig_g = Column(
        Float(),
        nullable=False,
    )
    sigma = Column(
        Float(),
        nullable=False,
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.dt, self.a_gsm, self.sig_a_gsm,
            self.a10, self.sig_a10, self.b, self.sig_b,
            self.g, self.sig_g, self.sigma
        )


class EarthParameters(LTVBase):
    __tablename__ = "earth_parameters"

    _REPR = (
        "{0}("
        " dt={1!r},"
        " ap={2!r},"
        " ap_max={3!r},"
        " dst={4!r},"
        " dst_min={5!r},"
        " ssc={6!r}"
        ")"
    )

    dt = Column(
        DateTime(timezone=False),
        primary_key=True
    )
    ap = Column(
        Float(),
        nullable=False,
        default=0
    )
    ap_max = Column(
        Float(),
        nullable=False,
        default=0
    )
    dst = Column(
        Float(),
        nullable=False,
        default=0
    )
    dst_min = Column(
        Float(),
        nullable=False,
        default=0
    )
    ssc = Column(
        Float(),
        nullable=False,
        default=0
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.dt, self.ap, self.ap_max,
            self.dst, self.dst_min, self.ssc
        )


class SolarParameters(LTVBase):
    __tablename__ = "solar_parameters"

    _REPR = (
        "{0}("
        " dt={1!r},"
        " w={2!r},"
        " tilt={3!r},"
        " cx={4!r},"
        " cme={5!r},"
        " bpole={6!r},"
        " bsign={7!r},"
        " bss={8!r},"
        " ach={9!r},"
        " f_10_7={10!r}"
        ")"
        )

    dt = Column(
        DateTime(timezone=False),
        primary_key=True
    )
    w = Column(
        Float(),
        nullable=False,
        default=0
    )
    tilt = Column(
        Float(),
        nullable=False,
        default=0
    )
    cx = Column(
        Float(),
        nullable=False,
        default=0
    )
    cme = Column(
        Float(),
        nullable=False,
        default=0
    )
    bpole = Column(
        Float(),
        nullable=False,
        default=0
    )
    bsign = Column(
        Integer(),
        nullable=False,
        default=0
    )
    bss = Column(
        Float(),
        nullable=False,
        default=0
    )
    ach = Column(
        Float(),
        nullable=False,
        default=0
    )
    f_10_7 = Column(
        Float(),
        nullable=False,
        default=0
    )

    def __repr__(self):
        return self._REPR.format(
            self.__class__.__name__, self.dt, self.w, self.tilt, self.cx,
            self.cme, self.bpole, self.bsign, self.bss, self.ach, self.f_10_7
        )
