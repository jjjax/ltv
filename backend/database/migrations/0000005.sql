-- Finished unfinished tables
-- Improved integrity
-- Improved performance
-- Added new columns for ltv.ltv.space

ALTER TABLE ltv.ltv.data ALTER COLUMN c SET DEFAULT 0;
ALTER TABLE ltv.ltv.data ALTER COLUMN eff_mod SET DEFAULT 0;
ALTER TABLE ltv.ltv.data ALTER COLUMN sig_mod SET DEFAULT 0;
ALTER TABLE ltv.ltv.data ALTER COLUMN eff_rel SET DEFAULT 0;
ALTER TABLE ltv.ltv.data ALTER COLUMN sig_rel SET DEFAULT 0;

CREATE INDEX IF NOT EXISTS btree__data__dt ON ltv.ltv.data (dt);


ALTER TABLE ltv.ltv.results ADD PRIMARY KEY (dt);

COMMENT ON COLUMN ltv.ltv.results.a10 IS '0th harmonic amplitude (%)';
COMMENT ON COLUMN ltv.ltv.results.sig_a10 IS 'sigma of a10 (%)';
COMMENT ON COLUMN ltv.ltv.results.b IS 'specter of rigidities parameter (Gv)';
COMMENT ON COLUMN ltv.ltv.results.sig_b IS 'sigma of b (%)';
COMMENT ON COLUMN ltv.ltv.results.g IS 'power of the specter of rigidities (gamma > 0)';
COMMENT ON COLUMN ltv.ltv.results.sig_g IS 'sigma of g (%)';
COMMENT ON COLUMN ltv.ltv.results.sigma IS 'sigma of the whole result (%)';


ALTER TABLE ltv.ltv.rigidities DROP CONSTRAINT rigidities__dt__check_july;

ALTER TABLE ltv.ltv.rigidities ALTER COLUMN rc SET DEFAULT 0;
ALTER TABLE ltv.ltv.rigidities ALTER COLUMN rm SET DEFAULT 0;
ALTER TABLE ltv.ltv.rigidities ALTER COLUMN re SET DEFAULT 0;

CREATE INDEX IF NOT EXISTS btree__rigidities__dt ON ltv.ltv.rigidities (dt);


ALTER TABLE ltv.ltv.space ADD PRIMARY KEY (dt);

ALTER TABLE ltv.ltv.space ADD COLUMN ach REAL DEFAULT 0 NOT NULL;
ALTER TABLE ltv.ltv.space ADD COLUMN ap REAL DEFAULT 0 NOT NULL;
ALTER TABLE ltv.ltv.space ADD COLUMN ap_max REAL DEFAULT 0 NOT NULL;
ALTER TABLE ltv.ltv.space ADD COLUMN dst REAL DEFAULT 0 NOT NULL;
ALTER TABLE ltv.ltv.space ADD COLUMN dst_max REAL DEFAULT 0 NOT NULL;
ALTER TABLE ltv.ltv.space ADD COLUMN ssc REAL DEFAULT 0 NOT NULL;

COMMENT ON COLUMN ltv.ltv.space.ach IS 'area of active solar regions (ppm)';
COMMENT ON COLUMN ltv.ltv.space.ap IS 'ap index';
COMMENT ON COLUMN ltv.ltv.space.ap_max IS 'ap_max index';
COMMENT ON COLUMN ltv.ltv.space.dst IS 'disturbance storm time index (nT)';
COMMENT ON COLUMN ltv.ltv.space.dst_max IS 'maximum disturbance storm time index (nT)';
COMMENT ON COLUMN ltv.ltv.space.ssc IS 'storm sudden commencement index';
COMMENT ON COLUMN ltv.ltv.space.bsign IS 'the sign of the magnetic field of the Sun (-1, 0, 1)';

ALTER TABLE ltv.ltv.space ALTER COLUMN w SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN tilt SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN cx SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN cme SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN kp SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN bpole SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN bsign SET DEFAULT 0;
ALTER TABLE ltv.ltv.space ALTER COLUMN bss SET DEFAULT 0;

ALTER TABLE ltv.ltv.space ADD CONSTRAINT chk__space__bsign__in_range CHECK (bsign IN (-1, 0 , 1));
