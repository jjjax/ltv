--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: ltv; Type: SCHEMA; Schema: -; Owner: ltv
--

CREATE SCHEMA ltv;


ALTER SCHEMA ltv OWNER TO ltv;

--
-- Name: SCHEMA ltv; Type: COMMENT; Schema: -; Owner: ltv
--

COMMENT ON SCHEMA ltv IS 'Dedicated schema for the Long Time Variations project';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = ltv, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: data; Type: TABLE; Schema: ltv; Owner: ltv
--

CREATE TABLE data (
    det_name character varying(30) NOT NULL,
    dir_name character varying(30) NOT NULL,
    dt timestamp without time zone NOT NULL,
    c real NOT NULL,
    eff_mod real NOT NULL,
    sig_mod real NOT NULL,
    eff_rel real NOT NULL,
    sig_rel real NOT NULL
);


ALTER TABLE data OWNER TO ltv;

--
-- Name: COLUMN data.c; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN data.c IS 'corrected data (Hz)';


--
-- Name: COLUMN data.eff_mod; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN data.eff_mod IS 'effectivity by the model (%)';


--
-- Name: COLUMN data.sig_mod; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN data.sig_mod IS 'sigma by the model (%)';


--
-- Name: COLUMN data.eff_rel; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN data.eff_rel IS 'effectivity by relations (%)';


--
-- Name: COLUMN data.sig_rel; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN data.sig_rel IS 'sigma by relations (%)';


--
-- Name: detectors; Type: TABLE; Schema: ltv; Owner: ltv
--

CREATE TABLE detectors (
    det_name character varying(30) NOT NULL,
    lat real NOT NULL,
    lon real NOT NULL,
    p0 real NOT NULL
);


ALTER TABLE detectors OWNER TO ltv;

--
-- Name: COLUMN detectors.lat; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN detectors.lat IS 'deg';


--
-- Name: COLUMN detectors.lon; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN detectors.lon IS 'deg';


--
-- Name: COLUMN detectors.p0; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN detectors.p0 IS 'mBar';


--
-- Name: directions; Type: TABLE; Schema: ltv; Owner: ltv
--

CREATE TABLE directions (
    det_name character varying(30) NOT NULL,
    dir_name character varying(30) NOT NULL,
    teta real NOT NULL,
    depth real NOT NULL
);


ALTER TABLE directions OWNER TO ltv;

--
-- Name: COLUMN directions.teta; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN directions.teta IS 'deg';


--
-- Name: COLUMN directions.depth; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN directions.depth IS 'mwe (meter water equivalent)';


--
-- Name: results; Type: TABLE; Schema: ltv; Owner: ltv
--

CREATE TABLE results (
    dt timestamp without time zone NOT NULL,
    a10 real NOT NULL,
    sig_a10 real NOT NULL,
    b real NOT NULL,
    sig_b real NOT NULL,
    g real NOT NULL,
    sig_g real NOT NULL,
    sigma real NOT NULL
);


ALTER TABLE results OWNER TO ltv;

--
-- Name: rigidities; Type: TABLE; Schema: ltv; Owner: ltv
--

CREATE TABLE rigidities (
    det_name character varying(30) NOT NULL,
    dir_name character varying(30) NOT NULL,
    dt timestamp without time zone NOT NULL,
    rc real NOT NULL,
    rm real NOT NULL,
    re real NOT NULL,
    CONSTRAINT rigidities__dt__check_july CHECK (((((date_part('year'::text, (dt)::date))::text || '-07-01 00:00:00'::text))::timestamp without time zone = dt))
);


ALTER TABLE rigidities OWNER TO ltv;

--
-- Name: COLUMN rigidities.rc; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN rigidities.rc IS 'rigidity cutoff (Gv)';


--
-- Name: COLUMN rigidities.rm; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN rigidities.rm IS 'rigidity median (Gv)';


--
-- Name: COLUMN rigidities.re; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN rigidities.re IS 'rigidity effective (Gv)';


--
-- Name: space; Type: TABLE; Schema: ltv; Owner: ltv
--

CREATE TABLE space (
    dt timestamp without time zone NOT NULL,
    w real NOT NULL,
    tilt real NOT NULL,
    cx real NOT NULL,
    cme real NOT NULL,
    kp real NOT NULL,
    bpole real NOT NULL,
    bsign smallint NOT NULL,
    bss real NOT NULL
);


ALTER TABLE space OWNER TO ltv;

--
-- Name: COLUMN space.w; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.w IS 'Wolf Number';


--
-- Name: COLUMN space.tilt; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.tilt IS 'the title of current sheet degree';


--
-- Name: COLUMN space.cx; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.cx IS 'cx index';


--
-- Name: COLUMN space.cme; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.cme IS 'the number of coronar mass ejections';


--
-- Name: COLUMN space.kp; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.kp IS 'kp index';


--
-- Name: COLUMN space.bpole; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.bpole IS 'magnetic field at the Magnetic Pole (Gauss)';


--
-- Name: COLUMN space.bsign; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.bsign IS 'the sign of the magnetic field of the Sun';


--
-- Name: COLUMN space.bss; Type: COMMENT; Schema: ltv; Owner: ltv
--

COMMENT ON COLUMN space.bss IS 'the magnetic field of the Sun as a Star (Gauss)';


--
-- Data for Name: data; Type: TABLE DATA; Schema: ltv; Owner: ltv
--

COPY data (det_name, dir_name, dt, c, eff_mod, sig_mod, eff_rel, sig_rel) FROM stdin;
\.


--
-- Data for Name: detectors; Type: TABLE DATA; Schema: ltv; Owner: ltv
--

COPY detectors (det_name, lat, lon, p0) FROM stdin;
\.


--
-- Data for Name: directions; Type: TABLE DATA; Schema: ltv; Owner: ltv
--

COPY directions (det_name, dir_name, teta, depth) FROM stdin;
\.


--
-- Data for Name: results; Type: TABLE DATA; Schema: ltv; Owner: ltv
--

COPY results (dt, a10, sig_a10, b, sig_b, g, sig_g, sigma) FROM stdin;
\.


--
-- Data for Name: rigidities; Type: TABLE DATA; Schema: ltv; Owner: ltv
--

COPY rigidities (det_name, dir_name, dt, rc, rm, re) FROM stdin;
\.


--
-- Data for Name: space; Type: TABLE DATA; Schema: ltv; Owner: ltv
--

COPY space (dt, w, tilt, cx, cme, kp, bpole, bsign, bss) FROM stdin;
\.


--
-- Name: data_pkey; Type: CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY data
    ADD CONSTRAINT data_pkey PRIMARY KEY (det_name, dir_name, dt);


--
-- Name: directions_pkey; Type: CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY directions
    ADD CONSTRAINT directions_pkey PRIMARY KEY (dir_name, det_name);


--
-- Name: rigidities_pkey; Type: CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY rigidities
    ADD CONSTRAINT rigidities_pkey PRIMARY KEY (det_name, dir_name, dt);


--
-- Name: stations__det_name; Type: CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY detectors
    ADD CONSTRAINT stations__det_name PRIMARY KEY (det_name);


--
-- Name: data_fkey; Type: FK CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY data
    ADD CONSTRAINT data_fkey FOREIGN KEY (det_name, dir_name) REFERENCES directions(det_name, dir_name) ON UPDATE CASCADE;


--
-- Name: directions_det_name_fkey; Type: FK CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY directions
    ADD CONSTRAINT directions_det_name_fkey FOREIGN KEY (det_name) REFERENCES detectors(det_name) ON UPDATE CASCADE;


--
-- Name: rigidities_det_name_dir_name_fkey; Type: FK CONSTRAINT; Schema: ltv; Owner: ltv
--

ALTER TABLE ONLY rigidities
    ADD CONSTRAINT rigidities_det_name_dir_name_fkey FOREIGN KEY (det_name, dir_name) REFERENCES directions(det_name, dir_name) ON UPDATE CASCADE;


--
-- Name: ltv; Type: ACL; Schema: -; Owner: ltv
--

REVOKE ALL ON SCHEMA ltv FROM PUBLIC;
REVOKE ALL ON SCHEMA ltv FROM ltv;
GRANT ALL ON SCHEMA ltv TO ltv;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

