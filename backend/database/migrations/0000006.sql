-- Tables results and space were rewritten
-- Enforced some CONSTRAINTs

DROP TABLE IF EXISTS ltv.ltv.results;
CREATE TABLE IF NOT EXISTS ltv.ltv.gsm_results
(
  dt      TIMESTAMP NOT NULL
    CONSTRAINT gsm_results_pkey
    PRIMARY KEY,
  a_gsm     REAL      NOT NULL,
  sig_a_gsm REAL      NOT NULL,
  a10       REAL      NOT NULL,
  sig_a10   REAL      NOT NULL,
  b         REAL      NOT NULL,
  sig_b     REAL      NOT NULL,
  g         REAL      NOT NULL,
  sig_g     REAL      NOT NULL,
  sigma     REAL      NOT NULL
);
COMMENT ON COLUMN gsm_results.a_gsm IS 'gsm result; variance of the model (%)';
COMMENT ON COLUMN gsm_results.a_gsm IS 'sigma of the gsm result (%)';
COMMENT ON COLUMN gsm_results.a10 IS '0th harmonic amplitude (%)';
COMMENT ON COLUMN gsm_results.sig_a10 IS 'sigma of a10 (%)';
COMMENT ON COLUMN gsm_results.b IS 'specter of rigidities parameter (Gv)';
COMMENT ON COLUMN gsm_results.sig_b IS 'sigma of b (%)';
COMMENT ON COLUMN gsm_results.g IS 'power of the specter of rigidities (gamma > 0)';
COMMENT ON COLUMN gsm_results.sig_g IS 'sigma of g (%)';
COMMENT ON COLUMN gsm_results.sigma IS 'sigma of the whole result (%)';


DROP TABLE IF EXISTS ltv.ltv.space;
CREATE TABLE IF NOT EXISTS ltv.ltv.solar_parameters
(
  dt      TIMESTAMP          NOT NULL
    CONSTRAINT solar_parameters_pkey
    PRIMARY KEY,
  w       REAL DEFAULT 0     NOT NULL,
  tilt    REAL DEFAULT 0     NOT NULL,
  cx      REAL DEFAULT 0     NOT NULL,
  cme     REAL DEFAULT 0     NOT NULL,
  bpole   REAL DEFAULT 0     NOT NULL,
  bsign   SMALLINT DEFAULT 0 NOT NULL,
  bss     REAL DEFAULT 0     NOT NULL,
  ach     REAL DEFAULT 0     NOT NULL,
  CONSTRAINT chk__space__bsign__in_range
    CHECK (bsign = ANY (ARRAY ['-1' :: INTEGER, 0, 1])),
  CONSTRAINT chk__solar_parameters__gt_zero CHECK (
    w >= 0::REAL
    AND tilt >= 0::REAL
    AND cme >= 0::REAL
    AND bpole >= 0::REAL
    AND bss >= 0::REAL
    AND ach >= 0::REAL
  )
);
COMMENT ON COLUMN ltv.ltv.solar_parameters.w IS 'Wolf Number';
COMMENT ON COLUMN ltv.ltv.solar_parameters.tilt IS 'the title of current sheet degree';
COMMENT ON COLUMN ltv.ltv.solar_parameters.cx IS 'cx index';
COMMENT ON COLUMN ltv.ltv.solar_parameters.cme IS 'the number of coronar mass ejections';
COMMENT ON COLUMN ltv.ltv.solar_parameters.bpole IS 'magnetic field at the Magnetic Pole (Gauss)';
COMMENT ON COLUMN ltv.ltv.solar_parameters.bsign IS 'the sign of the magnetic field of the Sun (-1, 0, 1)';
COMMENT ON COLUMN ltv.ltv.solar_parameters.bss IS 'the magnetic field of the Sun as a Star (Gauss)';
COMMENT ON COLUMN ltv.ltv.solar_parameters.ach IS 'area of active solar regions (ppm)';

CREATE TABLE IF NOT EXISTS ltv.ltv.earth_parameters
(
  dt      TIMESTAMP          NOT NULL
    CONSTRAINT earth_parameters_pkey
    PRIMARY KEY,
  kp      REAL DEFAULT 0     NOT NULL,
  ap      REAL DEFAULT 0     NOT NULL,
  ap_max  REAL DEFAULT 0     NOT NULL,
  dst     REAL DEFAULT 0     NOT NULL,
  dst_max REAL DEFAULT 0     NOT NULL,
  ssc     REAL DEFAULT 0     NOT NULL,
  CONSTRAINT chk__earth_parameters__gt_zero CHECK (
    kp >= 0::REAL
    AND ap >= 0::REAL
    AND ap_max >= 0::REAL
    AND ssc >= 0::REAL
  )
);
COMMENT ON COLUMN ltv.ltv.earth_parameters.kp IS 'kp index';
COMMENT ON COLUMN ltv.ltv.earth_parameters.ap IS 'ap index';
COMMENT ON COLUMN ltv.ltv.earth_parameters.ap_max IS 'ap_max index';
COMMENT ON COLUMN ltv.ltv.earth_parameters.dst IS 'disturbance storm time index (nT)';
COMMENT ON COLUMN ltv.ltv.earth_parameters.dst_max IS 'maximum disturbance storm time index (nT)';
COMMENT ON COLUMN ltv.ltv.earth_parameters.ssc IS 'storm sudden commencement index';

ALTER TABLE ltv.ltv.earth_parameters RENAME COLUMN dst_max TO dst_min;


ALTER TABLE ltv.ltv.solar_parameters ADD COLUMN f_10_7 REAL DEFAULT 0 NOT NULL;
COMMENT ON COLUMN ltv.ltv.solar_parameters.f_10_7 IS 'the 10.7cm solar radio flux (sfu)';

ALTER TABLE ltv.ltv.solar_parameters DROP CONSTRAINT IF EXISTS chk__solar_parameters__gt_zero;
ALTER TABLE IF EXISTS ltv.ltv.solar_parameters
ADD CONSTRAINT chk__solar_parameters__gt_zero
CHECK (
    w >= 0::REAL
    AND tilt >= 0::REAL
    AND cme >= 0::REAL
    AND bpole >= 0::REAL
    AND bss >= 0::REAL
    AND ach >= 0::REAL
    AND f_10_7 >= 0::REAL
);


ALTER TABLE ltv.ltv.earth_parameters DROP CONSTRAINT chk__earth_parameters__gt_zero;
ALTER TABLE ltv.ltv.earth_parameters DROP COLUMN kp;
ALTER TABLE ltv.ltv.earth_parameters ADD CONSTRAINT chk__earth_parameters__gt_zero
CHECK (
    ap >= 0::REAL
    AND ap_max >= 0::REAL
    AND ssc >= 0::REAL
);
