-- New naming scheme for CONSTRAINTs
-- CASCADing options for FOREIGN KEYs

ALTER TABLE ltv.ltv.data DROP CONSTRAINT data_fkey;
ALTER TABLE ltv.ltv.rigidities DROP CONSTRAINT rigidities_det_name_dir_name_fkey;
ALTER TABLE ltv.ltv.directions DROP CONSTRAINT directions_det_name_fkey;

ALTER TABLE ltv.ltv.rigidities
ADD CONSTRAINT fkey__data_to_directions__det_name_dir_name
FOREIGN KEY (det_name, dir_name)
REFERENCES ltv.ltv.directions(det_name, dir_name)
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ltv.ltv.directions
ADD CONSTRAINT fkey__directions_to_detectors__det_name
FOREIGN KEY (det_name)
REFERENCES ltv.ltv.detectors(det_name)
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ltv.ltv."data"
ADD CONSTRAINT fkey__data_to_directions__det_name_dir_name
FOREIGN KEY (det_name, dir_name)
REFERENCES directions (det_name, dir_name)
ON UPDATE CASCADE ON DELETE CASCADE;
