#!/usr/bin/env python3
import datetime
import configparser
import os
import glob
import re

import psycopg2
import psycopg2.extras

USER = "ltv"  # admin user

QUERY = (
    "INSERT INTO ltv.ltv.rigidities (det_name, dir_name, dt, rc) "
    "VALUES (%s, %s, %s, %s) "
    "ON CONFLICT (det_name, dir_name, dt) DO UPDATE  SET"
    " rc = EXCLUDED.rc"
)


def main(db_conf):
    with psycopg2.connect(**db_conf) as conn:
        with conn.cursor() as cursor:
            data = []
            for kind in ("mt", "nm"):
                path = "../data/initial/rigidities/{0}/*".format(kind)
                for filename in glob.iglob(path):

                    converted = unconverted = os.path.basename(filename).lower()
                    for sentinel in ["_igrf_", "_rceff", "_reff"]:
                        if sentinel in converted:
                            converted = unconverted.split(sentinel)[0]
                            break
                    if converted == unconverted:
                        raise ValueError(
                            "Cannot convert '{0}' detector name "
                            "dynamically.".format(unconverted)
                        )
                    det = kind + "_" + converted  # like, "nm_nagoya"
                    if kind == "mt":
                        det_name, dir_name, *_ = det.split(".")
                    else:
                        det_name, *_ = det.split(".")
                        dir_name = "v0"

                    with open(filename, mode="r", encoding="utf8") as fd:
                        for line in fd:
                            match = re.match(
                                r"^(\d+)"  # date
                                r"\s+?"
                                r"([\d.]+)",  # value
                                line
                            )
                            if not match:
                                continue

                            dt, value = match.groups()
                            if not float(value):
                                continue

                            # FIXME: currently we have yearly rigidities
                            # FIXME: but it's more convenient to have monthly
                            # FIXME: because of the joins with ORM
                            for m in range(1, 12+1):
                                dt_i = datetime.datetime(int(dt), m, 1, 0, 0, 0)
                                data.append((det_name, dir_name, dt_i, value))

                    # Test that the key (det_name, dir_name)
                    # is present in the table
                    cursor.execute(
                        "SELECT TRUE "
                        "FROM ltv.ltv.directions "
                        "WHERE det_name = %s AND dir_name = %s",
                        (det_name, dir_name)
                    )
                    result = cursor.fetchone()
                    if not result:
                        raise ValueError(
                            "There's no ({0}, {1}) PRIMARY KEY "
                            "in the ltv.ltv.directions table."
                            "".format(det_name, dir_name)
                        )

            psycopg2.extras.execute_batch(
                cursor,
                QUERY,
                data,
                page_size=2000
            )


if __name__ == "__main__":
    self = os.path.realpath(__file__)
    os.chdir(os.path.dirname(self))

    parser = configparser.ConfigParser()
    parser.read("../database/conn_setup.ini")

    conf = {"user": USER}
    conf.update(parser[USER])

    main(conf)
