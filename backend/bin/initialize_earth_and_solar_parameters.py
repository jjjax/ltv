#!/usr/bin/env python3
import configparser
import collections
import datetime
import os
import re
import glob

import psycopg2
import psycopg2.extras

PATH = "../data/initial/space/*"

LINE = re.compile(
    "^(\d{4}(?:\s+|-)\d{2}-\d{2}(?:\s+|T)\d{2}:\d{2}:\d{2})(?:\+\d+)?"  # date
    "\s+"
    "([\d.-]+)"  # value
)

USER = "ltv"  # admin user

EARTH_P = "dt,ap,ap_max,dst,dst_min,ssc".split(",")
SOLAR_P = "dt,w,tilt,cx,cme,bpole,bsign,bss,ach,f_10_7".split(",")

# The query is idempotent
QUERY_EARTH = (
    "INSERT INTO ltv.ltv.earth_parameters ("
    "  dt,"
    "  ap,"
    "  ap_max,"
    "  dst,"
    "  dst_min,"
    "  ssc"
    ") "
    "VALUES ("
    " %(dt)s, %(ap)s, %(ap_max)s, %(dst)s, %(dst_min)s, %(ssc)s"
    ") "
    "ON CONFLICT (dt) DO UPDATE SET"
    "  ap = EXCLUDED.ap,"
    "  ap_max = EXCLUDED.ap_max,"
    "  dst = EXCLUDED.dst,"
    "  dst_min = EXCLUDED.dst_min,"
    "  ssc = EXCLUDED.ssc"
)

# The query is idempotent
QUERY_SOLAR = (
    "INSERT INTO ltv.ltv.solar_parameters ("
    "  dt,"
    "  w,"
    "  tilt,"
    "  cx,"
    "  cme,"
    "  bpole,"
    "  bsign,"
    "  bss,"
    "  ach,"
    "  f_10_7"
    ") "
    "VALUES ("
    " %(dt)s, %(w)s, %(tilt)s, %(cx)s, %(cme)s,"
    "%(bpole)s, %(bsign)s, %(bss)s, %(ach)s, %(f_10_7)s"
    ") "
    "ON CONFLICT (dt) DO UPDATE SET"
    "  w = EXCLUDED.w,"
    "  tilt = EXCLUDED.tilt,"
    "  cx = EXCLUDED.cx,"
    "  cme = EXCLUDED.cme,"
    "  bpole = EXCLUDED.bpole,"
    "  bsign = EXCLUDED.bsign,"
    "  bss = EXCLUDED.bss,"
    "  ach = EXCLUDED.ach,"
    "  f_10_7 = EXCLUDED.f_10_7"
)

earth_data = collections.defaultdict(lambda: {
    p: 0 for p in EARTH_P
})
solar_data = collections.defaultdict(lambda: {
    p: 0 for p in SOLAR_P
})


def main(db_conf):
    for filename in glob.iglob(PATH):
        parameter = os.path.basename(filename).split(".")[0].lower()

        is_f_10_7 = parameter == "f_10_7"
        is_bsign = parameter == "bsign"
        is_tilt = parameter == "tilt"

        if parameter in EARTH_P:
            is_earth = True
        elif parameter in SOLAR_P:
            is_earth = False
        else:
            raise ValueError(
                "Unknown parameter '{0}' "
                "for the '{1}' file"
                "".format(parameter, filename)
            )

        with open(filename, "r", encoding="utf8") as fd:
            for line in fd:
                match = LINE.match(line)
                if not match:
                    continue

                dt, value = match.groups()
                if (is_f_10_7 or is_tilt) and float(value) < 0:
                    continue

                if is_bsign:
                    value = int(float(value))

                try:
                    dt = datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")
                except ValueError:
                    dt = datetime.datetime.strptime(dt, "%Y %m-%d %H:%M:%S")

                if is_earth:
                    earth_data[dt][parameter] = value
                    earth_data[dt]["dt"] = dt
                else:
                    solar_data[dt][parameter] = value
                    solar_data[dt]["dt"] = dt
    earth_values = earth_data.values()
    solar_values = solar_data.values()
    with psycopg2.connect(**db_conf) as conn:
        with conn.cursor() as cursor:
            psycopg2.extras.execute_batch(
                cursor,
                QUERY_EARTH,
                earth_values,
                page_size=1000
            )

            psycopg2.extras.execute_batch(
                cursor,
                QUERY_SOLAR,
                solar_values,
                page_size=1000
            )


if __name__ == "__main__":
    self = os.path.realpath(__file__)
    os.chdir(os.path.dirname(self))

    parser = configparser.ConfigParser()
    parser.read("../database/conn_setup.ini")

    conf = {"user": USER}
    conf.update(parser[USER])

    main(conf)
