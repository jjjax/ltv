#!/usr/bin/env python3
import configparser
import os
import re

import psycopg2
import psycopg2.extras

LINE = re.compile(
    r"^([-+])"  # availability
    r"\s+"
    r"([\w_]+)"  # det_name
    r"\s+"
    r"(\w+)"  # dir_name
    r"\s+"
    r"([-\d.]+)"  # latitude
    r"\s+"
    r"([-\d.]+)"  # longitude
    r"\s+\d+\s+"
    r"([\d.]+)"  # p0
    r"\s+"
    r"([\d.]+)"  # depth
    r"\s+"
    r"([\d.]+)",  # theta
)

USER = "ltv"  # admin user

# The query is idempotent
QUERY_DET = (
    "INSERT INTO ltv.ltv.detectors (det_name, lat, lon, p0)"
    " VALUES (%s, %s, %s, %s)"
    " ON CONFLICT (det_name) DO UPDATE SET"
    "  lat = EXCLUDED.lat,"
    "  lon = EXCLUDED.lon,"
    "  p0 = EXCLUDED.p0"
)

# The query is idempotent
QUERY_DIR = (
    "INSERT INTO ltv.ltv.directions (det_name, dir_name, theta, depth)"
    " VALUES (%s, %s, %s, %s)"
    " ON CONFLICT (det_name, dir_name) DO UPDATE SET"
    "  theta = EXCLUDED.theta,"
    "  depth = EXCLUDED.depth"
)


def main(db_conf):
    with psycopg2.connect(**db_conf) as conn:
        with conn.cursor() as cursor:
            detectors_data = []
            directions_data = []
            with open(
                    "../data/initial/nm_stations.txt",
                    mode="r",
                    encoding="utf8"
            ) as fd:
                for line in fd:
                    match = LINE.match(line)
                    if not match or match.groups()[0] == "-":
                        continue

                    det_name, dir_name, lat, lon, p0, d, t = match.groups()[1:]
                    det_name = "nm_" + det_name.lower()
                    detectors_data.append((det_name, lat, lon, p0))
                    directions_data.append((det_name, dir_name, t, d))

            psycopg2.extras.execute_batch(cursor, QUERY_DET, detectors_data)
            psycopg2.extras.execute_batch(cursor, QUERY_DIR, directions_data)

            # It's possible to combine the following statements
            # with the previous ones, but then we will lose readability
            detectors_data = []
            directions_data = []
            with open(
                    "../data/initial/mt_stations.txt",
                    mode="r",
                    encoding="utf8"
            ) as fd:
                current_det_name = ""
                for line in fd:
                    match = LINE.match(line)
                    if not match or match.groups()[0] == "-":
                        continue

                    det_name, dir_name, lat, lon, p0, d, t = match.groups()[1:]
                    det_name = "mt_" + det_name.lower()

                    if det_name != current_det_name:
                        current_det_name = det_name
                        detectors_data.append((det_name, lat, lon, p0))
                    directions_data.append((det_name, dir_name, t, d))

            psycopg2.extras.execute_batch(cursor, QUERY_DET, detectors_data)
            psycopg2.extras.execute_batch(cursor, QUERY_DIR, directions_data)


if __name__ == "__main__":
    self = os.path.realpath(__file__)
    os.chdir(os.path.dirname(self))

    parser = configparser.ConfigParser()
    parser.read("../database/conn_setup.ini")

    conf = {"user": USER}
    conf.update(parser[USER])

    main(conf)
