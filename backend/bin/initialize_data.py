#!/usr/bin/env python3
import configparser
import os
import glob
import re

import psycopg2
import psycopg2.extras

USER = "ltv"  # admin user

QUERY = (
    "INSERT INTO ltv.ltv.data ("
    "   det_name,"
    "   dir_name,"
    "   dt,"
    "   c"
    ")"
    " VALUES (%s, %s, %s, %s)"
    " ON CONFLICT (det_name, dir_name, dt) DO UPDATE SET"
    "  c = EXCLUDED.c"  # update corrected_data
)


def main(db_conf):
    with psycopg2.connect(**db_conf) as conn:
        with conn.cursor() as cursor:
            data = []
            for kind in ("mt", "nm"):
                path = "../data/initial/data/{0}/*".format(kind)
                for filename in glob.iglob(path):

                    det = os.path.basename(filename)
                    det = kind + "_" + det  # like, "nm_nagoya"
                    if kind == "mt":
                        det_name, dir_name, *_ = det.lower().split(".")
                    else:
                        det_name, *_ = det.lower().split(".")
                        dir_name = "v0"

                    with open(filename, mode="r", encoding="utf8") as fd:
                        for line in fd:
                            match = re.match(
                                r"^(\d+-\d+-\d+\s+\d+:\d+:\d+)(?:\+\d+)?"
                                r".+\s"
                                r"([\d.]+)",  # value
                                line
                            )
                            if not match:
                                continue

                            dt, value = match.groups()
                            if not float(value):
                                continue

                            data.append((det_name, dir_name, dt, value))

                    # Test that the key (det_name, dir_name)
                    # is present in the table
                    cursor.execute(
                        "SELECT TRUE "
                        "FROM ltv.ltv.directions "
                        "WHERE det_name = %s AND dir_name = %s",
                        (det_name, dir_name)
                    )
                    result = cursor.fetchone()
                    if not result:
                        raise ValueError(
                            "There's no ({0}, {1}) PRIMARY KEY "
                            "in the ltv.ltv.directions table."
                            "".format(det_name, dir_name)
                        )

            psycopg2.extras.execute_batch(
                cursor,
                QUERY,
                data,
                page_size=500
            )


if __name__ == "__main__":
    self = os.path.realpath(__file__)
    os.chdir(os.path.dirname(self))

    parser = configparser.ConfigParser()
    parser.read("../database/conn_setup.ini")

    conf = {"user": USER}
    conf.update(parser[USER])

    main(conf)
